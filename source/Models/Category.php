<?php


namespace Source\Models;


use Source\Core\Connect;

class Category
{

    private $db;

    public function __construct()
    {
        $this->db = Connect::getInstance();
    }

    public function findByAll(): ?array
    {

        try {

            $stmt = "SELECT * FROM category";
            $stmt = $this->db->prepare($stmt);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return $stmt->fetchAll();
            } else {
                return false;
            }


        } catch (\PDOException $exception) {
            echo "ERROR" . $exception->getMessage();
        }

    }


}