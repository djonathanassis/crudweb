<?php


namespace Source\Models;


use Source\Core\Model;

/**
 * Class Contact
 * @package Source\Models
 */
class Contact extends Model
{

    /**
     * @return array|null
     */
    public function findByAll(): ?array
    {
        try {
            $stmt = "SELECT co.id, co.name, co.email, co.message, ca.title FROM contact co LEFT JOIN category ca ON ca.id = co.id_category";
            $stmt = $this->db->prepare($stmt);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return $stmt->fetchAll();
            } else {
                return $stmt = [];
            }
        } catch (\PDOException $exception) {
            echo "ERROR: " . $exception->getMessage();
            exit;
        }

    }

    /**
     * @param $id
     * @return array|false|null
     */
    public function findById($id): ?array
    {
        try {
            $stmt = "SELECT * FROM contact WHERE id = :id";
            $stmt = $this->db->prepare($stmt);
            $stmt->bindValue(':id', $id,FILTER_DEFAULT);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return $stmt->fetchAll();
            }else{
                return false;
            }
        } catch (\PDOException $exception) {
            echo "ERROR: " . $exception->getMessage();
            exit;
        }

    }

    /**
     * @param $name
     * @param $email
     * @param $message
     * @param $id_category
     * @return bool|null
     */
    public function create($name, $email, $message, $id_category): ?bool
    {
        try {
            $stmt = "INSERT INTO contact (name, email, message, id_category ) VALUES (:name, :email, :message, :id_category)";
            $stmt = $this->db->prepare($stmt);
            $stmt->bindParam(":name", $name, FILTER_DEFAULT);
            $stmt->bindParam(":email", $email, FILTER_DEFAULT);
            $stmt->bindParam(":message", $message, FILTER_DEFAULT);
            $stmt->bindParam(":id_category", $id_category, FILTER_SANITIZE_NUMBER_INT);
            $stmt->execute();

            if($stmt->rowCount() > 0){
                return true;
            }else{
                return false;
            }

        } catch (\PDOException $exception) {
            echo "ERROR" . $exception->getMessage();
        }
    }

    /**
     * @param $id
     * @param $name
     * @param $email
     * @param $message
     * @param $id_category
     * @return bool|null
     */
    public function update($id, $name, $email, $message, $id_category): ?bool
    {

        try {
            $stmt = "UPDATE contact SET name = :name, email = :email, message = :message, id_category = :id_category WHERE id = :id";
            $stmt = $this->db->prepare($stmt);
            $stmt->bindParam(":id", $id, FILTER_SANITIZE_NUMBER_INT);
            $stmt->bindParam(":name", $name, FILTER_DEFAULT);
            $stmt->bindParam(":email", $email, FILTER_DEFAULT);
            $stmt->bindParam(":message", $message, FILTER_DEFAULT);
            $stmt->bindParam(":id_category", $id_category, FILTER_DEFAULT);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return true;
            } else {
                return false;
            }

        } catch (\PDOException $exception) {
            echo "ERROR: " . $exception->getMessage();
            exit;
        }

    }

    /**
     * @param $id
     * @return bool|null
     */
    public function delete($id): ?bool
    {
        try {
            $stmt = "DELETE FROM contact WHERE id = :id";
            $stmt = $this->db->prepare($stmt);
            $stmt->bindValue(':id', $id, FILTER_DEFAULT);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (\PDOException $exception) {
            echo "ERROR: " . $exception->getMessage();
            exit;
        }

    }

}