<div class='container'>
    <legend class="mt-4">
        <h4>Alterar Contatos</h4>
    </legend>

    <fieldset>
        <form action="" method="post" enctype='multipart/form-data'>

            <input type="hidden" class="form-control" id="id" name="id" value="<?=$contact[0]->id; ?>" readonly = true>
            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" class="form-control" id="name" name="name" value="<?=$contact[0]->name;?>" placeholder="Infome o Nome" required>
            </div>

            <div class="form-group">
                <label for="id_category">State</label>
                <select id="id_category" name="id_category" class="form-control">
                    <?php foreach ($category as $value):?>
                        <option value="<?=$value->id?>" <?php if($value->id == $contact[0]->id_category) echo 'selected="selected"' ?>><?=$value->title?></option>
                    <?php endforeach;?>
                </select>
            </div>

            <div class="form-group">
                <label for="email">E-mail</label>
                <input type="email" class="form-control" id="email" name="email" value="<?=$contact[0]->email;?>" placeholder="Informe o E-mail" required>
            </div>

            <div class="form-group">
                <label for="message">Menssagem</label>
                <textarea class="form-control" id="message" name="message" rows="3"><?=$contact[0]->message;?></textarea>
            </div>

            <input type="hidden" name="action" value="create">
            <button type="submit" class="btn btn-primary">
                Gravar
            </button>
            <a href='' class="btn btn-danger">Cancelar</a>
        </form>
    </fieldset>
</div>
