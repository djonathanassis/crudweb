<div class='container'>

    <legend class="text-center mt-4"><h1>Listagem de Contatos</h1></legend>
    <div class='clearfix'></div>

    <?php if (!empty($contact)):?>

        <table class="table table-striped">
            <tr class='active'>
                <th>Codigo</th>
                <th>Nome</th>
                <th>Categoria</th>
                <th>E-mail</th>
                <th>Açãos</th>
            </tr>
            <?php foreach ($contact as $value): ?>
                <tr>
                    <td><?= $value->id; ?></td>
                    <td><?= $value->name; ?></td>
                    <td><?= $value->title; ?></td>
                    <td><?= $value->email; ?></td>
                    <td>
                        <a class="btn btn-primary" href="<?php echo BASE_URL; ?>/contact/edit/<?= $value->id; ?>">Editar</a>
                        <a class="btn btn-danger" href="<?php echo BASE_URL; ?>/contact/remove/<?= $value->id; ?>"
                           onclick="return confirm('Deseja excluir ?')">Excluir
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    <?php else: ?>

        <h3 class="text-center text-primary">Não existem contatos cadastrados!</h3>
    <?php endif; ?>
    </fieldset>
</div>
