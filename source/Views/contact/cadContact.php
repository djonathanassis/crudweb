<div class='container'>
    <div class="content m-5">
        <legend class="mt-4">
            <h4 class="text-right">FALE CONOSCO</h4>
        </legend>

        <fieldset>
            <form action="" method="post" enctype='multipart/form-data'>

                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Infome o Nome" required>
                </div>

                <div class="form-group">
                    <label for="id_category">Categoria</label>
                    <select id="id_category" name="id_category" class="form-control">
                        <option selected>Escolha uma Categoria</option>
                        <?php foreach ($category as $value):?>
                            <option value="<?=$value->id?>"><?=$value->title?></option>
                        <?php endforeach;?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Informe o E-mail" required>
                </div>

                <div class="form-group">
                    <label for="message">Menssagem</label>
                    <textarea class="form-control" id="message" name="message" rows="3"></textarea>
                </div>

                <input type="hidden" name="action" value="create">
                <button type="submit" class="btn btn-primary ali-right" >
                    Enviar
                </button>
            </form>
        </fieldset>
    </div>
</div>