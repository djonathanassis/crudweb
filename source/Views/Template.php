<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href='<?= BASE_URL ?>/source/Views/assets/css/main.css' rel='stylesheet' type='text/css'>
    <title>Contatos</title>
</head>

<body>
    <header>
        <section class="m-0">
            <div class="container-fluid px-0">
                <div id="carousel1" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="<?= BASE_URL ?>/source/Views/assets/img/conteudo.jpg" height="300px" alt="conteudo1">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Slider Teste</h5>
                                <p>Nulla vitae elit libero, a phoretra augue nollis interdum</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?= BASE_URL ?>/source/Views/assets/img/conteudo.jpg" height="300px" alt="conteudo2">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Slider Teste</h5>
                                <p>Nulla vitae elit libero, a phoretra augue nollis interdum</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="<?= BASE_URL ?>/source/Views/assets/img/conteudo.jpg" height="300px" alt="conteudo3">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Slider Teste</h5>
                                <p>Nulla vitae elit libero, a phoretra augue nollis interdum</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel1" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">anterior</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel1" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">proximo</span>
                    </a>
                </div>
            </div>
        </section>
        <div class="bg-light">
            <div class="container py-2">
                <div class="row">
                    <div class="col-lg-6 text-center px-0">
                        <nav class="navbar navbar-expand navbar-light justify-content-center px-0">
                            <ul class="navbar-nav px-0">
                                <li class="nav-item"><a class="nav-link" href="#">HOME</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">EMPRESA</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">SERVICOS</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?= BASE_URL ?>/contact/all">LISTA DE CONTATOS</a></li>
                                <li class="nav-item"><a class="nav-link" href="#">FALE CONOSCO</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <?php $this->loadViewInTemplate($view, $viewData); ?>

    <footer class="bg-light">
        <div class="container pt-5">
            <div class="row">
                <div class="col-lg-4 col-6">
                    <h6>LOCALIZACAO</h6>
                    <ul class="list-unstyled text-secondary">
                        <li>Av Sao Paulo</li>
                        <li>Maringa</li>
                        <li>CEP: 000000000</li>
                    </ul>
                </div>
                <div class="col-lg-4 col-6">
                    <h6>FALE CONOSCO</h6>
                    <ul class="list-unstyled">
                        <li>3425-2344</li>
                        <li>3425-5664</li>
                        <li>3425-5445</li>
                        <li class="text-secondary">djonathanassis@gmail.com</li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <h6>REDE SOCIAIS</h6>
                    <ul class="list-unstyled">
                        <li><a class="btn btn-outline-secondary btn-sm btn-block mb-2" href="#">Fecebook</a></li>
                        <li><a class="btn btn-outline-secondary btn-sm btn-block mb-2" href="#">Twitter</a></li>

                    </ul>
                </div>
            </div>
            <hr class="" style="background-color: black"/>
            <div class="my-2 py-2">
                <p class="mb-1">Djonathan © 2019. Alguns direitos reservados</p>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
            integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
            crossorigin="anonymous"></script>
</body>

</html>