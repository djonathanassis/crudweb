<?php


namespace Source\Controllers;


use Source\Core\Controller;
use Source\Models\Category;
use Source\Models\Contact;

/**
 * Class ContactController
 * @package Source\Controllers
 */
class ContactController extends Controller
{

    /**
     * @var Contact
     */
    protected $contact;

    /**
     * @var Category
     */
    protected $category;

    /**
     * ContactController constructor.
     */
    public function __construct()
    {
        $this->contact = new Contact();
        $this->category = new Category();
    }

    /**
     * cadastro de contatos
     */
    public function index()
    {
        $data = [];
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $name        = filter_input(INPUT_POST, 'name', FILTER_DEFAULT);
            $email       = filter_input(INPUT_POST, 'email', FILTER_DEFAULT);
            $message     = filter_input(INPUT_POST, 'message', FILTER_DEFAULT);
            $id_category = filter_input(INPUT_POST, 'id_category', FILTER_DEFAULT);

            $this->contact->create($name, $email, $message, $id_category);
            header("Location:" . BASE_URL . "/contact");
        }

        $data["category"] = $this->category->findByAll();

        $this->loadTemplete('cadContact', $data);

    }

    /**
     * listar contatos
     */
    public function all()
    {
        $data = [];
        $data["contact"] = $this->contact->findByAll();
        $this->loadTemplete('contact', $data);
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        $data = [];
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $id          = filter_input(INPUT_POST, 'id', FILTER_DEFAULT);
            $name        = filter_input(INPUT_POST, 'name', FILTER_DEFAULT);
            $email       = filter_input(INPUT_POST, 'email', FILTER_DEFAULT);
            $message     = filter_input(INPUT_POST, 'message', FILTER_DEFAULT);
            $id_category = filter_input(INPUT_POST, 'id_category', FILTER_DEFAULT);

            $this->contact->update($id, $name, $email, $message, $id_category);
            header("Location:" . BASE_URL . "/contact");
        }

        $data["category"] = $this->category->findByAll();
        $data['contact'] =  $this->contact->findById($id);

        $this->loadTemplete('editContact', $data);
    }

    /**
     * @param $id
     */
    public function remove($id)
    {
        $data = [];
        $this->contact->delete($id);
        header("Location:" . BASE_URL . "/contact");

        $this->loadTemplete('contact', $data);

    }

}