<?php


namespace Source\Core;

use Source\Core\Connect;

/**
 * Class Model
 * @package Source\Core
 */
class Model
{
    /**
     * @var \PDO
     */
    protected $db;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->db = Connect::getInstance();
    }
}